
#ifndef __BC25_AEP_H__
#define __BC25_AEP_H__

#include "at.h"

#ifdef __cplusplus
extern "C" {
#endif

#define THE_DEFAULT_AEP_LIFETIME  (86400)

struct aep_param
{
	char ip[16];
	unsigned int port;
};
typedef struct aep_param aep_param_t;
extern aep_param_t g_aep_param;

typedef enum {
	
	NCFG_LIFETIME_MODE = 0,
	NCFG_DEEPSLEEP_WAKE= 1
}enum_ncfg_mode_t;

typedef enum {
	
	NMGS_TYPE_0=0,
	NMGS_TYPE_1,
	NMGS_TYPE_2,
	NMGS_TYPE_100=100,
	NMGS_TYPE_101,
	NMGS_TYPE_102
	
}enum_nmgs_type_t;

typedef enum {
	
	NNMI_BUFFER_NOT_URC = 0,
	NNMI_DIRECT_MODE,
	NNMI_BUFFER_URC
	
}enum_nnmi_status_t;

typedef enum {
	
	POWER_ENABLE_MODE = 0,
	POWER_DISABLE_MODE= 1
}enum_power_mode_t;

typedef enum
{
	EVENT_START = 0,
	
	EVENT_FINDSIMCARD_ERR,				// 查PIN失败
	
	NET_EVENT_NET_ERR,					// 网络错误
	
	NCDPOPEN_CONNECT_EVENT_SUCC,		// 连接平台成功
	NCDPOPEN_CONNECT_EVENT_FAIL,		// 连接平台异常
	NCDPOPEN_NOURC_EVENT_ERR,			// 无URC上报
	
	NCDPOPEN_OBSERD_EVENT_SUCC,			// 订阅消息成功(只有等订阅消息成功后，才能NCDPSEND)

	NMGS_SEND_DATA_EVENT_SUCC,			// 发送数据失败
	NMGS_SEND_DATA_EVENT_FAIL,			// 发送数据失败
	NMGS_SEND_DATA_NOURC_EVENT_ERR,		// 发送数据没有urc
	
	NMGS_SERVER_REST_EVENT_ERR,			// 服务器下发rest
	
	DEEPSLEEP_WAKEUP_EVENT_SUCC,		// 睡眠唤醒后，网络恢复成功
	DEEPSLEEP_WAKEUP_EVENT_ERR,			// 睡眠唤醒后，网络恢复失败
	
	EVENT_END
	
} event_except_info_t;
extern event_except_info_t g_except_info;

typedef enum
{
	ERROR_IND = -1,					//error in when connect
	CONNECT_SUC = 0,				//reg success
	CONNECT_FAIL = 1,               //reg fail
	CONNECTED_OBSERD = 3,			//observed obj 19/0/0, can send msg
	MSG_SEND_SUC = 4,				//con msg send success
	MSG_SEND_FAIL = 5,				//con msg send failed
	RECOV_SUC_FROM_SLEEP = 6,		//recover connect success from deepsleep
	RECOV_FAIL_FROM_SLEEP = 7,		//recover connect fail from deepsleep, need reopen
	OBSV_FOTA_OBJ = 8,				//fota obj 5/0/3 has been obseverd
	DISOBSV_FOTA_OBJ = 9,			//fota obj 5/0/3 has been disobseverd
	RECV_RST_MSG = 10,				//revieve RST msg, need to close and reopen
	
} enum_qlwevtind_state_t;


void bc25_init(void);
void bc25_reset(void);
int8_t bc25_connect(void);
void bc25_power(enum_power_mode_t power_mode);

int8_t bc25_aep_ncfg(enum_ncfg_mode_t ncfg_mode,uint32_t life_time);
int8_t bc25_aep_ncdpopen(char* ip,uint16_t port);
int8_t bc25_aep_nmgs(void);
int8_t bc25_aep_nnmi(enum_nnmi_status_t nnmi_status);
int8_t bc25_aep_ncdpclose(void);

void set_except_event(event_except_info_t except_info);

#ifdef __cplusplus
}
#endif

#endif /* __BC25_AEP_H__*/
