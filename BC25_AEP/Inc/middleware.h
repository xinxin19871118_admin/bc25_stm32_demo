
#ifndef __MIDDLEWARE_H__
#define __MIDDLEWARE_H__

#include "stm32f1xx_hal.h"

#ifdef __cplusplus
extern "C" {
#endif

uint8_t get_str(char *src_string,  char *dest_string, unsigned char index);

#ifdef __cplusplus
}
#endif

#endif /* __MIDDLEWARE_H__ */
