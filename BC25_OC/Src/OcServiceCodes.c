#include <ctype.h>
#include "OcServiceCodes.h"
#include "dbg_log.h"


//无符号整型16位  
uint_16 aep_htons(uint_16 source)  
{  

	if(OC_ENDIANNESS == OC_BIG_ENDIAN)
		return source;
	else
		return (uint_16)( 0
		| ((source & 0x00ff) << 8)
		| ((source & 0xff00) >> 8) );  
}  

//无符号整型32位
uint_32 aep_htoni(uint_32 source)  
{  
	if(OC_ENDIANNESS == OC_BIG_ENDIAN)
		return source;
	else
		return 0
		| ((source & 0x000000ff) << 24)
		| ((source & 0x0000ff00) << 8)
		| ((source & 0x00ff0000) >> 8)
		| ((source & 0xff000000) >> 24);  
}

//无符号整型64位
uint_64 aep_htonl(uint_64 source)  
{  
	if(OC_ENDIANNESS == OC_BIG_ENDIAN)
		return source;
	else
		return 0
		| ((source & (uint_64)(0x00000000000000ff)) << 56)
		| ((source & (uint_64)(0x000000000000ff00)) << 40)
		| ((source & (uint_64)(0x0000000000ff0000)) << 24)
		| ((source & (uint_64)(0x00000000ff000000)) << 8)
		| ((source & (uint_64)(0x000000ff00000000)) >> 8)
		| ((source & (uint_64)(0x0000ff0000000000)) >> 24)
		| ((source & (uint_64)(0x00ff000000000000)) >> 40)
		| ((source & (uint_64)(0xff00000000000000)) >> 56);
}

//float
float aep_htonf(float source)  
{  
	if(OC_ENDIANNESS == OC_BIG_ENDIAN)
		return source;
	else
	{
		uint_32 t= 0
			| ((*(uint_32*)&source & 0x000000ff) << 24)
			| ((*(uint_32*)&source & 0x0000ff00) << 8)
			| ((*(uint_32*)&source & 0x00ff0000) >> 8)
			| ((*(uint_32*)&source & 0xff000000) >> 24);
		return *(float*)&t;
	} 
}

//double
double aep_htond(double source)  
{  
	if(OC_ENDIANNESS == OC_BIG_ENDIAN)
		return source;
	else
	{
		uint_64 t= 0
			| ((*(uint_64*)&source & (uint_64)(0x00000000000000ff)) << 56)
			| ((*(uint_64*)&source & (uint_64)(0x000000000000ff00)) << 40)
			| ((*(uint_64*)&source & (uint_64)(0x0000000000ff0000)) << 24)
			| ((*(uint_64*)&source & (uint_64)(0x00000000ff000000)) << 8)
			| ((*(uint_64*)&source & (uint_64)(0x000000ff00000000)) >> 8)
			| ((*(uint_64*)&source & (uint_64)(0x0000ff0000000000)) >> 24)
			| ((*(uint_64*)&source & (uint_64)(0x00ff000000000000)) >> 40)
			| ((*(uint_64*)&source & (uint_64)(0xff00000000000000)) >> 56);
		return *(double*)&t;
	}
}

//16进制转字符串
void HexToStr(char *pbDest, char *pbSrc, int nLen)
{
	unsigned char ddl,ddh;
	int i;

	for (i=0; i<nLen; i++)
	{
		ddh = 48 + (unsigned char)pbSrc[i] / 16;
		ddl = 48 + (unsigned char)pbSrc[i] % 16;
		if (ddh > 57) ddh = ddh + 7;
		if (ddl > 57) ddl = ddl + 7;
		pbDest[i*2] = ddh;
		pbDest[i*2+1] = ddl;
	}

	//pbDest[nLen*2] = '\0';
}

//字符串转16进制
void StrToHex(char *pbDest, char *pbSrc, int nLen)
{
	unsigned char h1,h2;
	unsigned char s1,s2;
	int i;

	for (i=0; i<nLen; i++)
	{
		h1 = pbSrc[2*i];
		h2 = pbSrc[2*i+1];

		s1 = toupper(h1) - 0x30;
		if (s1 > 9) 
			s1 -= 7;

		s2 = toupper(h2) - 0x30;
		if (s2 > 9) 
			s2 -= 7;

		pbDest[i] = s1*16 + s2;
	}
}

//数据上报:业务数据上报
OcString data_report_CodeDataReport (data_report srcStruct)
{
	char* index;
	OcString resultStruct;
	unsigned short tempLen;

	unsigned short payloadLen = 9;
	resultStruct.len = (1 + 2 + 2 + payloadLen) * 2;
	resultStruct.str = (char *)malloc(resultStruct.len + 1);
	memset(resultStruct.str, 0, resultStruct.len + 1);

	srcStruct.temperature = aep_htoni(srcStruct.temperature);

	index = resultStruct.str;

	memcpy(index, "02", 2);
	index += 1 * 2;

	tempLen = aep_htons(1);//服务ID
	HexToStr(index, (char *)&tempLen, 2);
	index += 2 * 2;

	tempLen = aep_htons(payloadLen);
	HexToStr(index, (char *)&tempLen, 2);
	index += 2 * 2;

	HexToStr(index, (char *)&srcStruct.temperature, 4);
	index += 4 * 2;

	HexToStr(index, (char *)&srcStruct.fan_left_right, 1);
	index += 1 * 2;

	HexToStr(index, (char *)&srcStruct.fan_up_down, 1);
	index += 1 * 2;

	HexToStr(index, (char *)&srcStruct.onoff_state, 1);
	index += 1 * 2;

	HexToStr(index, (char *)&srcStruct.mode, 1);
	index += 1 * 2;

	HexToStr(index, (char *)&srcStruct.current_speed, 1);
	index += 1 * 2;


	return resultStruct;
}

//事件上报:故障上报
OcString error_code_report_CodeEventReport (error_code_report srcStruct)
{
	char* index;
	OcString resultStruct;
	unsigned short tempLen;

	unsigned short payloadLen = 1;
	resultStruct.len = (1 + 2 + 2 + payloadLen) * 2;
	resultStruct.str = (char *)malloc(resultStruct.len + 1);
	memset(resultStruct.str, 0, resultStruct.len + 1);


	index = resultStruct.str;

	memcpy(index, "07", 2);
	index += 1 * 2;

	tempLen = aep_htons(1001);//服务ID
	HexToStr(index, (char *)&tempLen, 2);
	index += 2 * 2;

	tempLen = aep_htons(payloadLen);
	HexToStr(index, (char *)&tempLen, 2);
	index += 2 * 2;

	HexToStr(index, (char *)&srcStruct.error_code, 1);
	index += 1 * 2;


	return resultStruct;
}

//指令下发:设定运行状态
int set_run_DecodeCmdDown (char* source, set_run* dest)
{
	char* index = source;
	int srcStrLen = strlen(source);
	int len = 9;


	memset(dest, 0, sizeof(set_run));

	StrToHex((char *)&dest->onoff_state, index, 1);
	index += 1 * 2;

	StrToHex((char *)&dest->mode, index, 1);
	index += 1 * 2;

	StrToHex((char *)&dest->current_speed, index, 1);
	index += 1 * 2;

	StrToHex((char *)&dest->fan_up_down, index, 1);
	index += 1 * 2;

	StrToHex((char *)&dest->fan_left_right, index, 1);
	index += 1 * 2;

	StrToHex((char *)&dest->temperature, index, 4);
	dest->temperature = aep_htoni(dest->temperature);
	index += 4 * 2;

	if (len * 2 > srcStrLen)
	{
		return OC_CMD_PAYLOAD_PARSING_FAILED;
	}
	return OC_CMD_SUCCESS;
}

//指令下发响应:设定运行状态响应
OcString set_run_resp_CodeCmdResponse (set_run_resp srcStruct)
{
	char* index;
	OcString resultStruct;
	unsigned short tempLen;

	unsigned short payloadLen = 4;
	resultStruct.len = (1 + 2 + 2 + 2 + payloadLen) * 2;
	resultStruct.str = (char *)malloc(resultStruct.len + 1);
	memset(resultStruct.str, 0, resultStruct.len + 1);

	srcStruct.result = aep_htoni(srcStruct.result);

	index = resultStruct.str;

	memcpy(index, "86", 2);
	index += 1 * 2;

	tempLen = aep_htons(9001);//服务ID
	HexToStr(index, (char *)&tempLen, 2);
	index += 2 * 2;

	tempLen = aep_htons(srcStruct.taskId);//taskID
	HexToStr(index, (char *)&tempLen, 2);
	index += 2 * 2;

	tempLen = aep_htons(payloadLen);
	HexToStr(index, (char *)&tempLen, 2);
	index += 2 * 2;

	HexToStr(index, (char *)&srcStruct.result, 4);
	index += 4 * 2;


	return resultStruct;
}

//数据上报:信号数据上报
OcString signal_report_CodeDataReport (signal_report srcStruct)
{
	char* index;
	OcString resultStruct;
	unsigned short tempLen;
	unsigned short payloadLen = 20;
	
	resultStruct.len = (1 + 2 + 2 + payloadLen) * 2;
	resultStruct.str = (char *)malloc(resultStruct.len + 1);

	memset(resultStruct.str, 0, resultStruct.len + 1);

	srcStruct.rsrp = aep_htoni(srcStruct.rsrp);
	srcStruct.sinr = aep_htoni(srcStruct.sinr);
	srcStruct.pci = aep_htoni(srcStruct.pci);
	srcStruct.ecl = aep_htoni(srcStruct.ecl);
	srcStruct.cell_id = aep_htoni(srcStruct.cell_id);

		
	index = resultStruct.str;

	memcpy(index, "02", 2);
	index += 1 * 2;

	tempLen = aep_htons(2);//服务ID
	HexToStr(index, (char *)&tempLen, 2);
	index += 2 * 2;

	tempLen = aep_htons(payloadLen);
	HexToStr(index, (char *)&tempLen, 2);
	index += 2 * 2;

	HexToStr(index, (char *)&srcStruct.rsrp, 4);
	index += 4 * 2;

	HexToStr(index, (char *)&srcStruct.sinr, 4);
	index += 4 * 2;

	HexToStr(index, (char *)&srcStruct.pci, 4);
	index += 4 * 2;

	HexToStr(index, (char *)&srcStruct.ecl, 4);
	index += 4 * 2;

	HexToStr(index, (char *)&srcStruct.cell_id, 4);
	index += 4 * 2;
	

	return resultStruct;
}

OcCmdData decodeCmdDownFromStr(char* source)
{
	char* index;
	OcCmdData result;
	char cmdType;
	unsigned short serviceId;
	unsigned short payloadLen;

	memset(&result, 0, sizeof(OcCmdData));

	index = source;

	//解析指令类型
	StrToHex(&cmdType, index, 1);
	index += 1 * 2;
	if (cmdType != 0x06)
	{
		result.code = OC_CMD_INVALID_DATASET_TYPE;
	}

	//服务Id解析
	StrToHex((char *)&serviceId, index, 2);
	serviceId = aep_htons(serviceId);
	index += 2 * 2;

	StrToHex((char *)&result.taskId, index, 2);
	result.taskId = aep_htons(result.taskId);
	index += 2 * 2;

	//payload长度解析
	StrToHex((char *)&payloadLen, index, 2);
	payloadLen = aep_htons(payloadLen);
	index += 2 * 2;

	if (strlen(index) < payloadLen * 2)
	{
		result.code = OC_CMD_PAYLOAD_PARSING_FAILED;
		return result;
	}


	if (serviceId == 8001)
	{
		result.serviceIdentifier = "set_run";
		result.data = malloc(sizeof(set_run));
		memset(result.data, 0, sizeof(set_run));
		result.code = set_run_DecodeCmdDown(index, (set_run*)result.data);
	}
	else 
	{
		result.serviceIdentifier = NULL;
		result.data = malloc(payloadLen);
		memset(result.data, 0, sizeof(payloadLen));
		StrToHex((char *)result.data, index, payloadLen);
		result.code = OC_CMD_INVALID_DATASET_IDENTIFIER;
	}

	return result;
}

OcCmdData decodeCmdDownFromBytes(char* source, int len)
{
	char * str = malloc(len * 2 + 1);
	OcCmdData result;
	HexToStr(str, source, len);
	str[len * 2] = 0;
	
	result = decodeCmdDownFromStr(str);
	free(str);
	return result;
}

OcString codeDataReportByIdToStr (int serviceId, void * srcStruct)
{
	if (serviceId == 1)
	{
		return data_report_CodeDataReport(*(data_report*)srcStruct);
	}
	else if (serviceId == 1001)
	{
		return error_code_report_CodeEventReport(*(error_code_report*)srcStruct);
	}
	else if (serviceId == 9001)
	{
		return set_run_resp_CodeCmdResponse(*(set_run_resp*)srcStruct);
	}
	else if (serviceId == 2)
	{
		return signal_report_CodeDataReport(*(signal_report*)srcStruct);
	}
	else 
	{
		OcString result = {0};
		return result;
	}
}

OcBytes codeDataReportByIdToBytes(int serviceId, void * srcStruct)
{
	OcString temp = codeDataReportByIdToStr(serviceId, srcStruct);
	OcBytes result = {0};
	result.len = temp.len / 2;
	if (result.len > 0)
	{
		result.str = malloc(result.len);
		StrToHex(result.str, temp.str, result.len);
		free(temp.str);
	}
	return result;
}

OcString codeDataReportByIdentifierToStr (char* serviceIdentifier, void * srcStruct)
{
	if (strcmp(serviceIdentifier, "data_report") == 0)
	{
		return data_report_CodeDataReport(*(data_report*)srcStruct);
	}
	else if (strcmp(serviceIdentifier, "error_code_report") == 0)
	{
		return error_code_report_CodeEventReport(*(error_code_report*)srcStruct);
	}
	else if (strcmp(serviceIdentifier, "set_run_resp") == 0)
	{
		return set_run_resp_CodeCmdResponse(*(set_run_resp*)srcStruct);
	}
	else if (strcmp(serviceIdentifier, "signal_report") == 0)
	{
		return signal_report_CodeDataReport(*(signal_report*)srcStruct);
	}
	else 
	{
		OcString result = {0};
		return result;
	}
}

OcBytes codeDataReportByIdentifierToBytes(char* serviceIdentifier, void * srcStruct)
{
	OcString temp = codeDataReportByIdentifierToStr(serviceIdentifier, srcStruct);
	OcBytes result = {0};
	result.len = temp.len / 2;
	if (result.len > 0)
	{
		result.str = malloc(result.len);
		StrToHex(result.str, temp.str, result.len);
		free(temp.str);
	}
	return result;
}

