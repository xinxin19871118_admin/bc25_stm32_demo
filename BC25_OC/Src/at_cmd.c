/****************************************************************************
*
* Copy right: 2020-, Copyrigths of Quectel Ltd.
****************************************************************************
* File name: at_cmd.c
* History: Rev1.0 2020-12-1
****************************************************************************/

#include "at_cmd.h"

#include "dbg_log.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

/**
 * @description: 模组软重启
 * @param: none
 * @return 执行结果码
 */
int8_t at_set_qrst(void)
{
	int8_t result = Q_EOK;

	result = at_send_cmd(AT_QRST, AT_OK, 0, 1000);
	if(result != Q_EOK)
	{
		LOG_I("check_send_cmd: %s is error,result: %d !",AT_QRST, result);
	}
	return result;	
}

/**
 * @description: 关闭回显
 * @param: none
 * @return 执行结果码
 */
int8_t at_turn_off_echo(void)
{
	int8_t result = Q_EOK;

	result = at_send_cmd(ATE, AT_OK, 0, 1000);
	if(result != Q_EOK)
	{
		LOG_I("check_send_cmd: %s is error,result: %d !",ATE, result);
	}
	return result;	
}

/**
 * @description: 设置UE功能
 * @param: cfun_mode cfun模式		CFUN_ENABLE 	 打开射频
 * 									CFUN_DISABLE	关闭射频
 * @return 执行结果码
 */
int8_t at_set_cfun(enum_cfun_mode_t cfun_mode)
{
	char send_cmd[50];
	int8_t result = Q_EOK;
	
	sprintf(send_cmd,AT_CFUN,cfun_mode);
	
	result = at_send_cmd(send_cmd, AT_OK, 0, 90000);
	if(result != Q_EOK)
	{
		LOG_I("check_send_cmd: %s is error,result: %d !",send_cmd, result);
	}
	
	return result;
}

/**
 * @description: 清频点
 * @param: none
 * @return 执行结果码
 */
int8_t at_set_clean_frequency(void)
{
	int8_t result = Q_EOK;
	
	result = at_set_cfun(CFUN_DISABLE);
	if(result != Q_EOK)
	{
		LOG_I("at+cfun=%d,result:%d",CFUN_DISABLE,result);
		return result;
	}
	result = at_send_cmd(AT_QCSEARFCN, AT_OK, 0, 1000);
	if(result != Q_EOK)
	{
		LOG_I("check_send_cmd: %s is error,result: %d !",AT_QCSEARFCN, result);
		return result;
	}	
	result = at_set_cfun(CFUN_ENABLE);
	if(result != Q_EOK)
	{
		LOG_I("at+cfun=%d,result:%d",CFUN_DISABLE,result);
		return result;
	}
	return result;
}

/**
 * @description: 查询PIN
 * @param: none
 * @return 执行结果码
 */
int8_t at_query_cpin(void)
{
	int8_t result = Q_EOK;
	int count = 10;

	while(count--)
	{
		if( Q_EOK == at_send_cmd(AT_CPIN, AT_OK, 0, 5000) && at_resp_get_line_by_kw("+CPIN: READY") != NULL)
		{
			result = Q_EOK;
			break;
		}
		else
		{
			result = Q_ERROR;
			HAL_Delay(3000);
		}
	}
	return result;
}

/**
 * @description: 查询网络信号
 * @param: none
 * @return 执行结果码
 */
int8_t at_query_csq(void)
{
	int8_t result = Q_EOK;
	
	result = at_send_cmd(AT_CSQ, AT_OK, 0, 1000);
	if(result != Q_EOK)
	{
		LOG_I("check_send_cmd: %s is error,result: %d !",AT_CSQ, result);
	}	
	return result;
}

/**
 * @description: 查询工程模式
 * @param: qeng_info	qeng参数
 * @return 执行结果码
 */
int8_t at_query_qeng(at_qeng_info_t* qeng_info)
{
	int8_t result = Q_EOK;
	const char* tmp_str = NULL;
	const char s[2] = ",";
	char *token = NULL;
	char line_num = 0;
	
	result = at_send_cmd(AT_QENG, AT_OK, 0, 1000);
	if(result != Q_EOK)
	{
		LOG_I("check_send_cmd: %s is error,result: %d !",AT_QENG, result);
	}	
	else
	{
		if( NULL == qeng_info)
			return Q_ENOMEM;

		memset(qeng_info,0,sizeof(at_qeng_info_t));
		
		/* 该指针数据在下次发送at之前会被释放 */
		tmp_str = at_resp_get_line_by_kw("+QENG: ");
		
		if(NULL == tmp_str)
		{
			return Q_ENOMEM;
		}
		else
		{
			LOG_I("%s",tmp_str);
		}
		
		token = strtok((char* )tmp_str, s);
		while(token != NULL ) 
		{	
			// line_num 从0开始计数
			line_num ++;
			if ( 3 == line_num )
			{
				qeng_info->sc_pci = atoi(token);
			}
			else if (4 == line_num )
			{
				memset(qeng_info->sc_cellid,0,10);
				memcpy(qeng_info->sc_cellid,(token+1),(strlen(token)-2));
			}
			else if (5 == line_num )
			{
				qeng_info->sc_rsrp = atof(token);
			}
			else if ( 8 == line_num )
			{
				qeng_info->sc_snr = atoi(token);
			}
			else if ( 11 == line_num )
			{
				qeng_info->sc_ecl = atoi(token);
			}
			else
			{
				/* code */
			}
			token = strtok(NULL, s);
		}
	}
	return result;
}

/**
 * @description: 查询网IMEI/sn码
 * @param: cgsn_snt 		MEI_SNT		查询IMEI
 * 							SN_SNT		查询SN 
 * @return 执行结果码
 */
int8_t at_query_cgsn(enum_cgsn_mode_t cgsn_snt)
{
	int8_t result = Q_EOK;
	char send_cmd[50];
	const char *imei_str = NULL;

	sprintf(send_cmd,AT_CGSN,cgsn_snt);
	
	result = at_send_cmd(send_cmd, AT_OK, 0, 1000);
	if(result != Q_EOK)
	{
		LOG_I("check_send_cmd: %s is error,result: %d !",send_cmd, result);
	}
	else
	{
		/* 该指针数据在下次发送at之前会被释放 */
		imei_str = at_resp_get_line_by_kw("+CGSN: ");
		if(imei_str != NULL)
		{
			LOG_I("imei_str: %s",imei_str);
		}
	}
	return result;
}

/**
 * @description: 设置睡眠模式
 * @param: psm_mode 			PSM_ENABLE		使能
 * 								PSM_DISABLE		失能
 * @return 执行结果码
 */
int8_t at_set_qsclk(enum_psm_mode_t psm_mode)
{
	int8_t result = Q_EOK;
	char send_cmd[50];

	sprintf(send_cmd,AT_QSCLK,psm_mode);
	
	result = at_send_cmd(send_cmd, AT_OK, 0, 1000);
	if(result != Q_EOK)
	{
		LOG_I("check_send_cmd: %s is error,result: %d !",send_cmd, result);
	}
	return result;	
}

/**
 * @description: 省电模式设置
 * @param: cpsms_mode 				CPSMS_ENABLE	使能	
 * 									CPSMS_DISABLE	失能
 * @return 执行结果码
 */
int8_t at_set_cpsms(enum_cpsms_mode_t cpsms_mode)
{
	int8_t result = Q_EOK;
	char send_cmd[50];

	sprintf(send_cmd,AT_CPSMS,cpsms_mode);
	
	result = at_send_cmd(send_cmd, AT_OK, 0, 1000);
	if(result != Q_EOK)
	{
		LOG_I("check_send_cmd: %s is error,result: %d !",send_cmd, result);
	}
	return result;	
}

/**
 * @description: eDRX 
 * @param: cedrxs_mode 		CEDRXS_ENABLE	使能
 * 							CEDRXS_DISABLE	失能
 * @return 执行结果码
 */
int8_t at_set_cedrxs(enum_cedrxs_mode_t cedrxs_mode)
{
	int8_t result = Q_EOK;
	char send_cmd[50];

	sprintf(send_cmd,AT_CEDRXS,cedrxs_mode);
	
	result = at_send_cmd(send_cmd, AT_OK, 0, 1000);
	if(result != Q_EOK)
	{
		LOG_I("check_send_cmd: %s is error,result: %d !",send_cmd, result);
	}
	return result;	
}

/**
 * @description: 模 块 关机 
 * @param: qpwd_mode	0  紧急关机（不发送 URC NORMAL POWER DOWN）
 *						1  正常关机（执行从网络去附着的动作，并发送 URC NORMAL POWER DOWN）
 *						2  重启模块
 * @return 执行结果码
 */
int8_t at_set_qpowd(enum_qpwd_mode_t qpwd_mode)
{
	int8_t result = Q_EOK;
	char send_cmd[50];

	sprintf(send_cmd,AT_QPOWD,qpwd_mode);
	
	result = at_send_cmd(send_cmd, AT_OK, 0, 2000);
	if(result != Q_EOK)
	{
		LOG_I("check_send_cmd: %s is error,result: %d !",send_cmd, result);
	}
	return result;	
}

/**
 * @description: 获取网络下发IP
 * @param None
 * @return 	Q_EOK 	成功
 * 			OTHER	失败
 */ 
int8_t at_query_cgpaddr(void)
{	
	int8_t result = Q_EOK;
	int8_t count = 20;
	const char* ip_str = NULL;
	
	while(count > 0 && Q_EOK != at_send_cmd(AT_IPADDR, AT_IPADDR_SUCC, 0, 3000))
	{
		HAL_Delay(5000);
		count--;
	}
	if(0 == count)
	{
		LOG_D("<-- get localIPAddress failed -->");
		result = Q_ERROR;
	}
	else
	{
		LOG_D("<-- Get localIPAddress successfully -->");
		
		ip_str = at_resp_get_line_by_kw("+CGPADDR: 1,");
		if(ip_str != NULL)
		{
			LOG_I("<-- get localIP: %s -->",(ip_str+12));
		}
		result = Q_EOK;
	}
	return result;
}
