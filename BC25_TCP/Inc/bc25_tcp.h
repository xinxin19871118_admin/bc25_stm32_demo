
#ifndef __BC25_TCP_H__
#define __BC25_TCP_H__

#include "at.h"

#ifdef __cplusplus
extern "C" {
#endif


typedef enum
{
    SOCKET_SERVICE_TYPE_TCP = 0,   // TCP
    SOCKET_SERVICE_TYPE_UDP        // UDP  
}enum_socket_service_type_t;

typedef enum
{
    SOCKET_PROTOCOL_TYPE_IPV4 = 0,   // ipv4
    SOCKET_PROTOCOL_TYPE_IPV6        // ipv6
}enum_socket_protocol_type_t;

typedef enum
{
    SOCKET_MODE_BUFFER = 0,   		// 0  Buffer 模式
    SOCKET_MODE_DIRECT_PUSH,        // 1 Direct Push 模式
}enum_socket_access_mode_t;


struct tcp_param
{
	char ip[16];
	unsigned int port;
	
	unsigned short context_id;   // context ID, range is 1-3
  	unsigned short connect_id;   // socket service index, range is 0-4
	
	enum_socket_service_type_t service_type;
	enum_socket_protocol_type_t protocol_type;	
	enum_socket_access_mode_t access_mode;
};
typedef struct tcp_param tcp_param_t;
extern tcp_param_t g_tcp_param;


typedef enum {
	
	POWER_ENABLE_MODE = 0,
	POWER_DISABLE_MODE= 1
}enum_power_mode_t;

typedef enum
{
	EVENT_START = 0,
	EVENT_FINDSIMCARD_ERR,				// 查PIN失败
	NET_EVENT_NET_ERR,					// 网络错误
	
	QIOPEN_CONNECT_EVENT_SUCC,			// 连接平台成功
	QIOPEN_CONNECT_EVENT_FAIL,			// 连接平台异常
	QIOPEN_NOURC_EVENT_ERR,				// 无URC上报
	
	QISEND_SEND_DATA_EVENT_SUCC,		// 发送数据失败
	QISEND_SEND_DATA_EVENT_FAIL,		// 发送数据失败
	QISEND_SEND_DATA_NOURC_EVENT_ERR,	// 发送数据没有urc
	
	QICLOSE_EVENT_FAIL,					// 关闭失败
	QICLOSE_EVENT,						// 连接断开	
	
	EVENT_END
	
} event_except_info_t;
extern event_except_info_t g_except_info;

void bc25_init(void);
void bc25_reset(void);
int8_t bc25_connect(void);
void bc25_power(enum_power_mode_t power_mode);

int8_t bc25_tcp_qicfg(void);
int8_t bc25_tcp_qiopen(void);
int8_t bc25_tcp_qiclose(void);
int8_t bc25_tcp_send(uint8_t* data,uint16_t len);


void set_except_event(event_except_info_t except_info);

#ifdef __cplusplus
}
#endif

#endif /* __BC25_TCP_H__*/
