
#ifndef __DBG_LOG_H__
#define __DBG_LOG_H__


#include "stm32f1xx_hal.h"


#define DBG_BUFF_LEN		(512)
#define USE_DEBUG_ASSERT


#ifdef  USE_DEBUG_ASSERT
#define dbg_assert_param(expr) ((expr) ? (void)0U : dbg_assert_failed((uint8_t *)__FILE__, __LINE__))
#else
#define assert_param(expr) ((void)0U)
#endif /* USE_DEBUG_ASSERT */

void dbg_print(const char *msg,
            const char *func,   
            const int   line,
            const char *fmt,...);
void dbg_print_hex_buf(const char *msg, ...);
void dbg_print_buf(const char *msg, ...);

/* 打印应用信息 */
/* DBG_INFO("123456"); -> [INFO][main():0148] 123456 */
#define LOG_I(...) dbg_print("INFO", __FUNCTION__, __LINE__, __VA_ARGS__)
/* 打印应用调试信息 */
/* DBG_INFO("123456"); -> [DEBUG][main():0148] 123456 */
#define LOG_D(...) dbg_print("DEBUG", __FUNCTION__, __LINE__, __VA_ARGS__)	
/* 打印ERR信息 */
/* DBG_ERR("123456"); -> [ERR][main():0148] 123456 */
#define LOG_E(...) dbg_print("ERR", __FUNCTION__, __LINE__, __VA_ARGS__)	
/* HEX 数据打印 */
/* DBG_HEX("123456"); -> [HEX][123456][L:6] 0x31 0x32 0x33 0x34 0x35 0x36 */
#define LOG_HEX(...) dbg_print_hex_buf("HEX", __VA_ARGS__)
/* SEND_AT数据打印 */
#define DBG_SEND_AT(...) dbg_print_buf("SEND_AT",__VA_ARGS__)

void dbg_assert_failed(uint8_t* file, uint32_t line);
#endif /* __DBG_LOG_H__ */
