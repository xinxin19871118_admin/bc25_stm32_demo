/****************************************************************************
*
* Copy right: 2020-, Copyrigths of Quectel Ltd.
****************************************************************************
* File name: middleware.c
* History: Rev1.0 2020-12-1
****************************************************************************/

#include "middleware.h"

#include "at.h"
#include "string.h"


uint8_t get_str(char *src_string,  char *dest_string, unsigned char index)
{
    uint32_t SentenceCnt = 0;
    uint32_t ItemSum = 0;
    uint32_t ItemLen = 0, Idx = 0;
    uint32_t len = 0;
    unsigned int i = 0;
    
    if (src_string == NULL)
    {
        return FALSE;
    }
	
    len = strlen(src_string);
	for ( i = 0; i < len; i++)
	{
		if (*(src_string + i) == ',')
		{
			ItemLen = i - ItemSum - SentenceCnt;
			ItemSum  += ItemLen;
			if (index == SentenceCnt)
			{
				if (ItemLen == 0)
				{
					return FALSE;
				}
				else
				{
					memcpy(dest_string, src_string + Idx, ItemLen);
					*(dest_string + ItemLen) = '\0';
					return TRUE;
				}
			}
			SentenceCnt++; 	 
			Idx = i + 1;
		}		
	}
    if (index == SentenceCnt && (len - Idx) != 0)
    {
        memcpy(dest_string, src_string + Idx, len - Idx);
        *(dest_string + len) = '\0';
        return TRUE;
    }
    else 
    {
        return FALSE;
    }
}
