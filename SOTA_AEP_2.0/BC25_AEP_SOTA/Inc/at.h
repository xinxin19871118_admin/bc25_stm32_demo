
#ifndef __AT_H__
#define __AT_H__

#include "stm32f1xx_hal.h"

#define BUFF_SIZE_MAX 		2048
#define AT_CMD_MAX_LEN 		128


#define TRUE 		1
#define FALSE 		0


#define Q_EOK                         0               /* */
#define Q_ERROR                      -1               /* */
#define Q_ETIMEOUT                   -2               /* 超时*/
#define Q_EFULL                      -3               /* buffer 空间不足或buff满 */
#define Q_ENOMEM                     -4               /* 内存空间申请错误 */
#define Q_EBUSY                      -5               /* Busy */
#define Q_EMPTY						 -6				  /* 数据为空 */	

#define AT_RESP_END_OK                 "OK"
#define AT_RESP_END_ERROR              "ERROR"
#define AT_RESP_END_FAIL               "FAIL"
#define AT_END_CR_LF                   "\r\n"

extern struct ringbuffer g_uart_rxcb;

struct at_response
{
    /*  响应缓冲区 */
    char buf[BUFF_SIZE_MAX];
    /*  最大响应缓冲区大小 */
    uint16_t buf_size;
    /* 当前响应缓冲区的长度，即实际缓存长度 */
    uint16_t buf_len;
    /* 设置响应行数
    	==0 ：收到“确定”或“错误”后，响应数据将自动返回
     	!=0 : 接收到设置行号数据时返回响应数据*/
    uint16_t line_num;
    /* 收到的响应行数 */
    uint16_t line_counts;
    /* 最大响应时间 */
    uint32_t timeout;
	
};
typedef struct at_response at_response_t;

enum at_resp_status
{
    AT_STATUS_UNINITIALIZED = 1,
    AT_RESP_OK = 0,                   /* AT response end is OK */
    AT_RESP_ERROR = -1,               /* AT response end is ERROR */
    AT_RESP_TIMEOUT = -2,             /* AT response is timeout */
    AT_RESP_BUFF_FULL= -3,            /* AT response buffer is full */
};
typedef enum at_resp_status at_resp_status_t;

struct at_info
{
    /* 结束符 */
    char end_sign;
    /* 当前接收到一行数据缓冲区 */
    char recv_line_buf[BUFF_SIZE_MAX];
    /* 当前接收的一行数据的长度 */
    uint16_t recv_line_len;
    /*  支持的最大接收数据长度 */
    uint16_t recv_bufsz;
    /*  send 锁，为后续队列发送准备，为防止多次发送造成干扰 */
    uint8_t lock;
    /*  期待等到的响应 */
    at_response_t* resp;
    /*  响应的通知标志位 */
    uint8_t resp_notice;
    /*  响应的状态 */
    at_resp_status_t resp_status;
    /*  URC 查找表表 */
    struct at_urc_table *urc_table;
    /*  URC查找表长度 */
    uint16_t urc_table_size;

} ;
typedef struct at_info at_info_t;

struct at_urc
{
    const char *cmd_prefix;
    const char *cmd_suffix;
    void (*func)(const char *data, uint16_t size);
};
typedef struct at_urc *at_urc_t;

struct at_urc_table
{
    size_t urc_size;
    const struct at_urc *urc;
};
typedef struct at_urc *at_urc_table_t;

__weak void at_send_module(uint8_t* send_buf, uint16_t buf_len);

void at_init(void);
int at_send_cmd(const char* cmd, const char* resp_expr,
                const uint16_t lines, const uint32_t timeout);
void at_parser(void);

const char *at_resp_get_line_by_kw(const char *keyword);
int at_set_urc_table(const struct at_urc *urc_table, uint16_t table_sz);
int8_t at_get_rb_data(char *buf, uint32_t size);

void test_unit(void);

#endif /* __AT_H__ */
