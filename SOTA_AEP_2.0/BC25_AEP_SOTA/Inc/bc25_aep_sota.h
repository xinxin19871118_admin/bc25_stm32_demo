
#ifndef __BC25_AEP_SOTA_H__
#define __BC25_AEP_SOTA_H__

#include "at.h"
#include "stm32f1xx_hal.h"

#ifdef __cplusplus
extern "C" {
#endif

//|   区域名称    | flash占用大小  | flash开始地址 | flash结束地址 |       备注       |
//| :-----------: | :------------: | :-----------: | :-----------: | :--------------: |
//| STM32F103ZET6 | 512KB(0x80000) |   0x8000000   |   0x8080000   | stm32_FLASH空间  |
//|  BootLoader   |  16kb(0x4000)  |   0x8000000   |   0x8004000   |   软件引导程序   |
//|     APP1      | 64kb(0x10000)  |   0x8004000   |   0x8014000   |   用于存放APP1   |
//|   TEMP_APP    | 64kb(0x10000)  |   0x8014000   |   0x8024000   | 用于存放TEMP_APP |
//|   NV 数据区   |   2kb(0x800)   |   0x8024000   |   0x8024002   | 用于存放关键数据 |

//|     APP3      | 64kb(0x10000)  |   0x8025000   |   0x8040000   |   用于数据对比   |

//#define FLASH_BASE 		0x8000000.
#define BootLoader_ADDR 	FLASH_BASE
#define BootLoader_SIZE 	0x4000

#define FLASH_APP_ADDR 				(FLASH_BASE+BootLoader_SIZE)
#define FLASH_APP_SIZE 				0x10000

#define FLASH_APP_TEMP_ADDR 		(FLASH_APP_ADDR+FLASH_APP_SIZE)
#define FLASH_APP_TEMP_SIZE 		0x10000

#define FLASH_NV_ADDR 				(FLASH_APP_TEMP_ADDR+FLASH_APP_TEMP_SIZE)
#define FLASH_NV_SIZE 				0x800

#define PCP_HEAD_TYPEY					(uint16_t)0xFFFE				// PCP协议头部标识高字节
#define PCP_SOTA_VERSION				(uint8_t)0x01					// PCP协议版本

//状态机  -> 描述当前状态  (空闲状态、下载状态)   (一个字节 hex数据)
//总包数	-> 描述待下载总包数量					(两个字节)
//单包字节数 -> 单包下发字节数				2	
//已下载包数 -> 为断点续传准备、防止下载过程中 出现网络问题、断电等异常问题 2
//old版本号 -> 描述当前软件版本    16
//new版本号 -> 描述下载的software ver  16
//失败次数  -> 多次断电续传 还不成功 则直接失败
//校验码 ->  续传的校验参数

typedef struct{
	
	uint8_t new_version[16];		// new版本号
	uint16_t single_package;		// 单包字节数
	uint16_t total_package;			// 总包数
	uint16_t ver_pack_code;			// 校验码		

} sota_new_ver_t;

typedef struct{

	uint8_t sota_status;			// 状态
	uint16_t download_pack;		  	// 已下载包数
	uint8_t fail_times;				// 失败次数 	bootloader中检查，是否是下载模式?
	uint8_t old_version[16];		// old版本号
	sota_new_ver_t sota_new_ver;
	
} sota_type_t;

typedef enum {
	
	RUN_MODE = 0x01,
	DOWNLOAD_MODE,
	UPGRADE_MODE
	
} mcu_run_status_t;

typedef struct{
	
	uint16_t frame_header;				// 帧头		(0xFFFE)
	uint8_t version;					// 版本号	(0x01)
	uint8_t message_code;				// 消息码
	uint16_t check_code;				// CRC校验码
	uint16_t length;					// 数据区长度
	uint8_t data[512];					// 数据区
} pcp_type_t;

typedef enum {
	
	QUERY_VERSION = 19,					// 查询设备版本 			19 0x13
	NEW_VERSION_INFORM,					// 新版本通知 				20 0x14
	REQUEST_UPGRADE_PACKAGE,			// 请求升级包 				21 0x15
	RE_UPGRADE_PACK_DOWNLOAD_STA,		// 上报升级包下载状态 		22 0x16
	EXCUTE_UPGRADE,						// 执行升级 				23 0x17
	RE_UPGRADE_RESULT					// 上报升级结果 			24 0x18

} pcp_message_code_type_t;

typedef enum __PCP_ACK__ {
	
	ALLOW_UPGRADE = 0x00,				// 允许升级 or 处理成功
	DEVICE_USING,						// 设备使用中
	CQS_BADLY,							// 信号质量差
	AREADY_NEW_VERSION,					// 已是最新版本
	POWER_NOT_ENOUGH,					// 电量不足
	FLASH_NOT_ENOUGH,					// 剩余空间不足
	DOWNLOAD_TIMEOUT,					// 下载超时
	UPDATE_PACKAGE_CHECK_FAULT,			// 升级包校验失败
	UPDATE_PACKAGE_NOT_SUPPORT,			// 升级包类型不支持
	MEMORY_NOT_ENOUGH,					// 内存不足
	INSTALL_UPDATE_PACKAGE_FAULT,		// 安装升级包失败
	INTERNAL_ERROR = 0x7F,				// 内部异常(表示无法识别的异常)
	PPDATE_TASK_NOT_RUN = 0x80,			// 升级任务不存在
	REQUIRE_FRAGM_NOT_EXIT = 0x81		// 指定的分片不存在   
	
} pcp_ack_type_t;

typedef struct
{
  uint8_t   endian;       // 记录当前CPU大小端模式
}cpu_info_t;

void bc25_aep_sota_data(char* pack_data);
int8_t download_data_packet(void);

#ifdef __cplusplus
}
#endif

#endif /* __BC25_AEP_SOTA_H__*/
