
#ifndef __MIDDLEWARE_H__
#define __MIDDLEWARE_H__

#include "stm32f1xx_hal.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef unsigned char byte;

#define AEP_BIG_ENDIAN 'b'
#define AEP_LITTLE_ENDIAN 'l'

static union { char c[4]; unsigned long mylong; } endian_test = {{ 'l', '?', '?', 'b' } };
#define AEP_ENDIANNESS ((char)endian_test.mylong)


uint16_t do_crc(uint8_t* data, int length);
uint8_t get_str(char *src_string,  char *dest_string, unsigned char index);
uint16_t hex_str_to_byte(char *str, uint8_t *out);
void str_to_hex(char *dest, char *src, int len);
void hex_to_str(char *dest, char *src, int len);

uint16_t aep_htons(uint16_t source);
uint32_t aep_htoni(uint32_t source);
uint64_t aep_htonl(uint64_t source);
float aep_htonf(float source);





#ifdef __cplusplus
}
#endif

#endif /* __MIDDLEWARE_H__ */
