/****************************************************************************
*
* Copy right: 2020-, Copyrigths of Quectel Ltd.
****************************************************************************
* File name: middleware.c
* History: Rev1.0 2020-12-1
****************************************************************************/

#include "middleware.h"

#include "at.h"
#include <string.h>
#include <ctype.h>

const unsigned short crc16_table[256] = {
        0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
        0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
        0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
        0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
        0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
        0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
        0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
        0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
        0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
        0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
        0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
        0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
        0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
        0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
        0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
        0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
        0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
        0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
        0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
        0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
        0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
        0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
        0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
        0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
        0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
        0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
        0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
        0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
        0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
        0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
        0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
        0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
};

char* find_string(char *line, uint32_t len,char *str)
{
    int32_t i;
    int32_t str_len;
    char *p;

    if ((NULL == line) || (NULL == str))
        return NULL;
    
    str_len = strlen(str);
    if(str_len > len)
    {
        return NULL;
    }

    p = line;
    for (i = 0;i < len - str_len + 1; i++)
    {
        if (0 == strncmp (p, str, str_len))
        {
            return p;
        }else{
            p++;
        }
    }
    return NULL;
}

uint8_t get_str(char *src_string,  char *dest_string, unsigned char index)
{
    uint32_t SentenceCnt = 0;
    uint32_t ItemSum = 0;
    uint32_t ItemLen = 0, Idx = 0;
    uint32_t len = 0;
    unsigned int i = 0;
    
    if (src_string == NULL)
    {
        return FALSE;
    }
	
    len = strlen(src_string);
	for ( i = 0; i < len; i++)
	{
		if (*(src_string + i) == ',')
		{
			ItemLen = i - ItemSum - SentenceCnt;
			ItemSum  += ItemLen;
			if (index == SentenceCnt)
			{
				if (ItemLen == 0)
				{
					return FALSE;
				}
				else
				{
					memcpy(dest_string, src_string + Idx, ItemLen);
					*(dest_string + ItemLen) = '\0';
					return TRUE;
				}
			}
			SentenceCnt++; 	 
			Idx = i + 1;
		}		
	}
    if (index == SentenceCnt && (len - Idx) != 0)
    {
        memcpy(dest_string, src_string + Idx, len - Idx);
        *(dest_string + len) = '\0';
        return TRUE;
    }
    else 
    {
        return FALSE;
    }
}

int is_big_endian(void) 
{ 
	int val = 0x01;
	char ch = *((char *)&val);
	return (ch == 0x01 ? 0 : 1);
}

/* AEP 官方提供的CRC16 校验  */
uint16_t do_crc(uint8_t* data, int length)
{
    int cnt;
    int crc_reg = 0x0000;
    for (cnt = 0; cnt < length; cnt++)
    {
        crc_reg = (crc_reg >> 8) ^ crc16_table[(crc_reg ^ *(data++)) & 0xFF];
    }
    return crc_reg;
}

uint16_t hex_str_to_byte(char *str, uint8_t *out)
{
	char *p = str;
	char high = 0, low = 0;
	int tmplen = strlen(p), cnt = 0;
	tmplen = strlen(p);
	while(cnt < (tmplen / 2))
	{
		high = ((*p > '9') && ((*p <= 'F') || (*p <= 'f'))) ? *p - 48 - 7 : *p - 48;
		low = (*(++ p) > '9' && ((*p <= 'F') || (*p <= 'f'))) ? *(p) - 48 - 7 : *(p) - 48;
		out[cnt] = ((high & 0x0f) << 4 | (low & 0x0f));
		p ++;
		cnt ++;
	}
	if(tmplen % 2 != 0) out[cnt] = ((*p > '9') && ((*p <= 'F') || (*p <= 'f'))) ? *p - 48 - 7 : *p - 48;

	return (tmplen / 2 + tmplen % 2);
}

void str_to_hex(char *dest, char *src, int len)
{
	unsigned char h1,h2;
	unsigned char s1,s2;
	int i;

	for (i=0; i<len; i++)
	{
		h1 = src[2*i];
		h2 = src[2*i+1];

		s1 = toupper(h1) - 0x30;
		if (s1 > 9) 
			s1 -= 7;

		s2 = toupper(h2) - 0x30;
		if (s2 > 9) 
			s2 -= 7;

		dest[i] = s1*16 + s2;
	}
}

//16进制转字符串
void hex_to_str(char *dest, char *src, int len)
{
	unsigned char ddl,ddh;
	int i;

	for (i=0; i<len; i++)
	{
		ddh = 48 + (unsigned char)src[i] / 16;
		ddl = 48 + (unsigned char)src[i] % 16;
		if (ddh > 57) ddh = ddh + 7;
		if (ddl > 57) ddl = ddl + 7;
		dest[i*2] = ddh;
		dest[i*2+1] = ddl;
	}
}

/* 大小端数据格式转换 */
uint16_t bl_endian_hex(uint16_t value)
{
	return ((value & 0x00FF) << 8 ) | ((value & 0xFF00) >> 8);
}


int16_t byte_to_hex_str(uint8_t byte_arr[],int arr_len, char* hex_str,uint16_t* hex_str_len)
{
	uint16_t i = 0,index = 0;
	for (i = 0; i < arr_len; i++)
	{
		char tmp_hex1;
		char tmp_hex2;
		int value = byte_arr[i];
		int v1 = value/16;
		int v2 = value % 16;
		if (v1 >= 0 && v1 <= 9)
			tmp_hex1 = (char)(48+v1);
		else
			tmp_hex1 = (char)(55+v1);
		if (v2 >= 0 && v2 <= 9)
			tmp_hex2 = (char)(48+v2);
		else
			tmp_hex2 = (char)(55+v2);
		if(*hex_str_len <= i){
			return -1;
		}
		hex_str[index++] = tmp_hex1;
		hex_str[index++] = tmp_hex2;
	}
	*hex_str_len = index;
	return 0 ;
}


//无符号整型16位  
uint16_t aep_htons(uint16_t source)  
{  

	if(AEP_ENDIANNESS == AEP_BIG_ENDIAN)
		return source;
	else
		return (uint16_t)( 0
		| ((source & 0x00ff) << 8)
		| ((source & 0xff00) >> 8) );  
}  

//无符号整型32位
uint32_t aep_htoni(uint32_t source)
{  
	if(AEP_ENDIANNESS == AEP_BIG_ENDIAN)
		return source;
	else
		return 0
		| ((source & 0x000000ff) << 24)
		| ((source & 0x0000ff00) << 8)
		| ((source & 0x00ff0000) >> 8)
		| ((source & 0xff000000) >> 24);  
}

//无符号整型64位
uint64_t aep_htonl(uint64_t source)  
{  
	if(AEP_ENDIANNESS == AEP_BIG_ENDIAN)
		return source;
	else
		return 0
		| ((source & (uint64_t)(0x00000000000000ff)) << 56)
		| ((source & (uint64_t)(0x000000000000ff00)) << 40)
		| ((source & (uint64_t)(0x0000000000ff0000)) << 24)
		| ((source & (uint64_t)(0x00000000ff000000)) << 8)
		| ((source & (uint64_t)(0x000000ff00000000)) >> 8)
		| ((source & (uint64_t)(0x0000ff0000000000)) >> 24)
		| ((source & (uint64_t)(0x00ff000000000000)) >> 40)
		| ((source & (uint64_t)(0xff00000000000000)) >> 56);
}

//float
float aep_htonf(float source)  
{  
	if(AEP_ENDIANNESS == AEP_BIG_ENDIAN)
		return source;
	else
	{
		uint32_t t= 0
			| ((*(uint32_t*)&source & 0x000000ff) << 24)
			| ((*(uint32_t*)&source & 0x0000ff00) << 8)
			| ((*(uint32_t*)&source & 0x00ff0000) >> 8)
			| ((*(uint32_t*)&source & 0xff000000) >> 24);
		return *(float*)&t;
	} 
}

//double
double aep_htond(double source)  
{  
	if(AEP_ENDIANNESS == AEP_BIG_ENDIAN)
		return source;
	else
	{
		uint64_t t= 0
			| ((*(uint64_t*)&source & (uint64_t)(0x00000000000000ff)) << 56)
			| ((*(uint64_t*)&source & (uint64_t)(0x000000000000ff00)) << 40)
			| ((*(uint64_t*)&source & (uint64_t)(0x0000000000ff0000)) << 24)
			| ((*(uint64_t*)&source & (uint64_t)(0x00000000ff000000)) << 8)
			| ((*(uint64_t*)&source & (uint64_t)(0x000000ff00000000)) >> 8)
			| ((*(uint64_t*)&source & (uint64_t)(0x0000ff0000000000)) >> 24)
			| ((*(uint64_t*)&source & (uint64_t)(0x00ff000000000000)) >> 40)
			| ((*(uint64_t*)&source & (uint64_t)(0xff00000000000000)) >> 56);
		return *(double*)&t;
	}
}
