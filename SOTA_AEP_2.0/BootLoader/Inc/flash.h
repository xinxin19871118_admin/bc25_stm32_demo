#ifndef __FLASH_H__
#define __FLASH_H__


#include "stm32f1xx.h"


extern void  FLASH_PageErase(uint32_t PageAddress);

#define STM32_FLASH_SIZE 	512 	 		//所选STM32的FLASH容量大小(单位为K)
#define STM32_FLASH_WREN 	1              	//使能FLASH写入(0，不是能;1，使能)
#define FLASH_WAITETIME  	50000          	//FLASH等待超时时间


void flash_read(uint32_t read_addr,uint16_t *buf,uint16_t read_len);
void flash_write(uint32_t write_addr,uint16_t *buf,uint16_t write_len);
void flash_write_nocheck(uint32_t write_addr,uint16_t *buf,uint16_t write_len);
uint16_t flash_read_halfword(uint32_t faddr);

void iap_write_appbin(uint32_t appxaddr,uint8_t *appbuf,uint32_t appsize);
//测试写入
void test_write(uint32_t write_addr,uint16_t write_data) ;		

void iap_load_app(uint32_t appxaddr);
#endif
