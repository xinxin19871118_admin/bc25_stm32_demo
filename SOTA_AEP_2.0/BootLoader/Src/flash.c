#include "flash.h"

//读取指定地址的半字(16位数据) 
//faddr:读地址 
//返回值:对应数据.
uint16_t flash_read_halfword(uint32_t faddr)
{
	return *(volatile uint16_t*)faddr; 
}

void flash_write_nocheck(uint32_t write_addr,uint16_t *buf,uint16_t write_len)   
{ 			 		 
	uint16_t i;
	for(i=0;i<write_len;i++)
	{
		HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD,write_addr,buf[i]);
	    write_addr+=2;//地址增加2.
	}  
} 

#define STM_SECTOR_SIZE	2048

uint16_t STMFLASH_BUF[STM_SECTOR_SIZE/2];//最多是2K字节

void flash_write(uint32_t write_addr,uint16_t *buf,uint16_t write_len)	
{
	uint32_t secpos;	   //扇区地址
	uint16_t secoff;	   //扇区内偏移地址(16位字计算)
	uint16_t secremain; //扇区内剩余地址(16位字计算)	   
 	uint16_t i;    
	uint32_t offaddr;   //去掉0X08000000后的地址
	
	if(write_addr<FLASH_BASE||(write_addr>=(FLASH_BASE+1024*STM32_FLASH_SIZE)))return;//非法地址
	
	HAL_FLASH_Unlock();					//解锁
	offaddr=write_addr-FLASH_BASE;		//实际偏移地址.
	secpos=offaddr/STM_SECTOR_SIZE;			//扇区地址  0~127 for STM32F103RBT6
	secoff=(offaddr%STM_SECTOR_SIZE)/2;		//在扇区内的偏移(2个字节为基本单位.)
	secremain=STM_SECTOR_SIZE/2-secoff;		//扇区剩余空间大小   
	if(write_len<=secremain)secremain=write_len;//不大于该扇区范围
	while(1) 
	{	
		flash_read(secpos*STM_SECTOR_SIZE+FLASH_BASE,STMFLASH_BUF,STM_SECTOR_SIZE/2);//读出整个扇区的内容
		for(i=0;i<secremain;i++)	//校验数据
		{
			if(STMFLASH_BUF[secoff+i]!=0XFFFF)break;//需要擦除  	  
		}
		if(i<secremain)				//需要擦除
		{
			FLASH_PageErase(secpos*STM_SECTOR_SIZE+FLASH_BASE);	//擦除这个扇区
			FLASH_WaitForLastOperation(FLASH_WAITETIME);            	//等待上次操作完成
			CLEAR_BIT(FLASH->CR, FLASH_CR_PER);							//清除CR寄存器的PER位，此操作应该在FLASH_PageErase()中完成！
																		//但是HAL库里面并没有做，应该是HAL库bug！
			for(i=0;i<secremain;i++)//复制
			{
				STMFLASH_BUF[i+secoff]=buf[i];	  
			}
			flash_write_nocheck(secpos*STM_SECTOR_SIZE+FLASH_BASE,STMFLASH_BUF,STM_SECTOR_SIZE/2);//写入整个扇区  
		}else 
		{
			FLASH_WaitForLastOperation(FLASH_WAITETIME);       	//等待上次操作完成
			flash_write_nocheck(write_addr,buf,secremain);//写已经擦除了的,直接写入扇区剩余区间. 
		}
		if(write_len==secremain)break;//写入结束了
		else//写入未结束
		{
			secpos++;				//扇区地址增1
			secoff=0;				//偏移位置为0 	 
		   	buf+=secremain;  	//指针偏移
			write_addr+=secremain*2;	//写地址偏移(16位数据地址,需要*2)	   
		   	write_len-=secremain;	//字节(16位)数递减
			if(write_len>(STM_SECTOR_SIZE/2))secremain=STM_SECTOR_SIZE/2;//下一个扇区还是写不完
			else secremain=write_len;//下一个扇区可以写完了
		}	 
	};	
	HAL_FLASH_Lock();		//上锁
}


//从指定地址开始读出指定长度的数据
//ReadAddr:起始地址
//pBuffer:数据指针
//NumToWrite:半字(16位)数
void flash_read(uint32_t read_addr,uint16_t *buf,uint16_t read_len)   	
{
	uint16_t i;
	for(i=0;i<read_len;i++)
	{
		buf[i]=flash_read_halfword(read_addr);//读取2个字节.
		read_addr+=2;//偏移2个字节.	
	}
}

//////////////////////////////////////////测试用///////////////////////////////////////////
//WriteAddr:起始地址
//WriteData:要写入的数据
void test_write(uint32_t write_addr,uint16_t write_data)   	
{
	flash_write(write_addr,&write_data,1);//写入一个字 
}


typedef  void (*iapfun)(void);				//定义一个函数类型的参数.
iapfun jump2app; 
__asm void MSR_MSP(uint32_t addr) 
{
    MSR MSP, r0 			//set Main Stack value
    BX r14
}

void iap_load_app(uint32_t appxaddr)
{
	if(((*(volatile uint32_t*)appxaddr)&0x2FFE0000)==0x20000000)	//检查栈顶地址是否合法.
	{ 
		jump2app=(iapfun)*(volatile uint32_t*)(appxaddr+4);		//用户代码区第二个字为程序开始地址(复位地址)		
		MSR_MSP(*(volatile uint32_t*)appxaddr);					//初始化APP堆栈指针(用户代码区的第一个字用于存放栈顶地址)
		jump2app();									//跳转到APP.
	}
}
